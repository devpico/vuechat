import Vue from 'vue'
import Router from 'vue-router'
import IllusionChat from '@/views/IllusionChat'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'IllusionChat',
      component: IllusionChat
    }
  ]
})
